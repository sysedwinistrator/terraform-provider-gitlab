//go:build acceptance
// +build acceptance

package provider

import (
	"fmt"
	"regexp"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/resource"
	"github.com/hashicorp/terraform-plugin-sdk/v2/terraform"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/gitlab-org/terraform-provider-gitlab/internal/provider/testutil"
)

// This test actually tests two different things:
// 1 - it tests that resources created with the old SDK implementation still work in the framework implementation
// 2 - it tests that the framework state migrator works properly, since 15.11.0 used state v0. That's why the config uses `project_id` instead of `project`
// This is because the old SDK resource handled a migration, and we can't assume every user has migrated, so we need to maintain that migration in the new
// framework resource.
func TestAccGitlabProjectLevelMRApprovals_UpgradeFromSDKToFramework(t *testing.T) {
	testutil.SkipIfCE(t)
	testProject := testutil.CreateProject(t)

	resource.ParallelTest(t, resource.TestCase{
		CheckDestroy: testAccCheckGitlabProjectLevelMRApprovalsDestroy,
		Steps: []resource.TestStep{
			{

				ExternalProviders: map[string]resource.ExternalProvider{
					"gitlab": {
						VersionConstraint: "~> 15.11.0",
						Source:            "gitlabhq/gitlab",
					},
				},
				Config: fmt.Sprintf(`
				resource "gitlab_project_level_mr_approvals" "foo" {
					project_id                                        = "%d"
					reset_approvals_on_push                        = true
					disable_overriding_approvers_per_merge_request = true
					merge_requests_author_approval                 = true
					merge_requests_disable_committers_approval     = true
					require_password_to_approve                    = true
				}
			`, testProject.ID),
			},
			{
				ProtoV6ProviderFactories: testAccProtoV6ProviderFactories,
				Config: fmt.Sprintf(`
				resource "gitlab_project_level_mr_approvals" "foo" {
					project                                        = "%d"
					reset_approvals_on_push                        = true
					disable_overriding_approvers_per_merge_request = true
					merge_requests_author_approval                 = true
					merge_requests_disable_committers_approval     = true
					require_password_to_approve                    = true
				}
			`, testProject.ID),
				PlanOnly: true,
			},
		},
	})
}

func TestAccGitlabProjectLevelMRApprovals_basic(t *testing.T) {
	testutil.SkipIfCE(t)

	var projectApprovals gitlab.ProjectApprovals
	testProject := testutil.CreateProject(t)

	resource.ParallelTest(t, resource.TestCase{
		ProtoV6ProviderFactories: testAccProtoV6ProviderFactories,
		CheckDestroy:             testAccCheckGitlabProjectLevelMRApprovalsDestroy,
		Steps: []resource.TestStep{
			{
				Config: fmt.Sprintf(`
					resource "gitlab_project_level_mr_approvals" "foo" {
						project                                        = "%d"
						reset_approvals_on_push                        = true
						disable_overriding_approvers_per_merge_request = true
						merge_requests_author_approval                 = true
						merge_requests_disable_committers_approval     = true
						require_password_to_approve                    = true
					}
				`, testProject.ID),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckGitlabProjectLevelMRApprovalsExists("gitlab_project_level_mr_approvals.foo", &projectApprovals),
					testAccCheckGitlabProjectLevelMRApprovalsAttributes(&projectApprovals, &testAccGitlabProjectLevelMRApprovalsExpectedAttributes{
						resetApprovalsOnPush:                      true,
						disableOverridingApproversPerMergeRequest: true,
						mergeRequestsAuthorApproval:               true,
						mergeRequestsDisableCommittersApproval:    true,
						requirePasswordToApprove:                  true,
					}),
				),
			},
			// Verify Import
			{
				ResourceName:      "gitlab_project_level_mr_approvals.foo",
				ImportState:       true,
				ImportStateVerify: true,
			},
			{
				Config: fmt.Sprintf(`
					resource "gitlab_project_level_mr_approvals" "foo" {
						project                                     = "%d"
						reset_approvals_on_push                        = false
						disable_overriding_approvers_per_merge_request = false
						merge_requests_author_approval                 = false
						merge_requests_disable_committers_approval     = false
						require_password_to_approve                    = false
					}
				`, testProject.ID),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckGitlabProjectLevelMRApprovalsExists("gitlab_project_level_mr_approvals.foo", &projectApprovals),
					testAccCheckGitlabProjectLevelMRApprovalsAttributes(&projectApprovals, &testAccGitlabProjectLevelMRApprovalsExpectedAttributes{
						resetApprovalsOnPush:                      false,
						disableOverridingApproversPerMergeRequest: false,
						mergeRequestsAuthorApproval:               false,
						mergeRequestsDisableCommittersApproval:    false,
						requirePasswordToApprove:                  false,
					}),
				),
			},
			// Verify Import
			{
				ResourceName:      "gitlab_project_level_mr_approvals.foo",
				ImportState:       true,
				ImportStateVerify: true,
			},
			{
				Config: fmt.Sprintf(`
					resource "gitlab_project_level_mr_approvals" "foo" {
						project                                     = "%d"
						reset_approvals_on_push                        = true
						disable_overriding_approvers_per_merge_request = true
						merge_requests_author_approval                 = true
						merge_requests_disable_committers_approval     = true
						require_password_to_approve                    = true
					}
				`, testProject.ID),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckGitlabProjectLevelMRApprovalsExists("gitlab_project_level_mr_approvals.foo", &projectApprovals),
					testAccCheckGitlabProjectLevelMRApprovalsAttributes(&projectApprovals, &testAccGitlabProjectLevelMRApprovalsExpectedAttributes{
						resetApprovalsOnPush:                      true,
						disableOverridingApproversPerMergeRequest: true,
						mergeRequestsAuthorApproval:               true,
						mergeRequestsDisableCommittersApproval:    true,
						requirePasswordToApprove:                  true,
					}),
				),
			},
			// Verify Import
			{
				ResourceName:      "gitlab_project_level_mr_approvals.foo",
				ImportState:       true,
				ImportStateVerify: true,
			},
		},
	})
}

func TestAccGitlabProjectLevelMRApprovals_basicWithNamespace(t *testing.T) {
	testutil.SkipIfCE(t)

	var projectApprovals gitlab.ProjectApprovals
	testProject := testutil.CreateProject(t)

	resource.ParallelTest(t, resource.TestCase{
		ProtoV6ProviderFactories: testAccProtoV6ProviderFactories,
		CheckDestroy:             testAccCheckGitlabProjectLevelMRApprovalsDestroy,
		Steps: []resource.TestStep{
			{
				Config: fmt.Sprintf(`
					resource "gitlab_project_level_mr_approvals" "foo" {
						project                                        = "%s"
						reset_approvals_on_push                        = true
						disable_overriding_approvers_per_merge_request = true
						merge_requests_author_approval                 = true
						merge_requests_disable_committers_approval     = true
						require_password_to_approve                    = true
					}
				`, testProject.PathWithNamespace),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckGitlabProjectLevelMRApprovalsExists("gitlab_project_level_mr_approvals.foo", &projectApprovals),
					testAccCheckGitlabProjectLevelMRApprovalsAttributes(&projectApprovals, &testAccGitlabProjectLevelMRApprovalsExpectedAttributes{
						resetApprovalsOnPush:                      true,
						disableOverridingApproversPerMergeRequest: true,
						mergeRequestsAuthorApproval:               true,
						mergeRequestsDisableCommittersApproval:    true,
						requirePasswordToApprove:                  true,
					}),
				),
			},
			// Verify Import
			{
				ResourceName:      "gitlab_project_level_mr_approvals.foo",
				ImportState:       true,
				ImportStateVerify: true,
			},
		},
	})
}

func TestAccGitlabProjectLevelMRApprovals_selectiveCodeOwnerValidation(t *testing.T) {
	testutil.SkipIfCE(t)

	var projectApprovals gitlab.ProjectApprovals
	testProject := testutil.CreateProject(t)

	resource.ParallelTest(t, resource.TestCase{
		ProtoV6ProviderFactories: testAccProtoV6ProviderFactories,
		CheckDestroy:             testAccCheckGitlabProjectLevelMRApprovalsDestroy,
		Steps: []resource.TestStep{
			{ // Error because both are set to "true" and expect the error
				Config: fmt.Sprintf(`
					resource "gitlab_project_level_mr_approvals" "foo" {
						project                                        = "%s"
						reset_approvals_on_push                        = true
						disable_overriding_approvers_per_merge_request = true
						merge_requests_author_approval                 = true
						merge_requests_disable_committers_approval     = true
						require_password_to_approve                    = true
						selective_code_owner_removals                  = true
					}
				`, testProject.PathWithNamespace),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckGitlabProjectLevelMRApprovalsExists("gitlab_project_level_mr_approvals.foo", &projectApprovals),
					testAccCheckGitlabProjectLevelMRApprovalsAttributes(&projectApprovals, &testAccGitlabProjectLevelMRApprovalsExpectedAttributes{
						resetApprovalsOnPush:                      true,
						disableOverridingApproversPerMergeRequest: true,
						mergeRequestsAuthorApproval:               true,
						mergeRequestsDisableCommittersApproval:    true,
						requirePasswordToApprove:                  true,
					}),
				),
				ExpectError: regexp.MustCompile("selective_code_owner_removals can only be enabled when reset_approvals_on_push is disabled"),
			},
			{ // enable the selective code owner removals
				Config: fmt.Sprintf(`
					resource "gitlab_project_level_mr_approvals" "foo" {
						project                                        = "%s"
						reset_approvals_on_push                        = false
						disable_overriding_approvers_per_merge_request = true
						merge_requests_author_approval                 = true
						merge_requests_disable_committers_approval     = true
						require_password_to_approve                    = true
						selective_code_owner_removals                  = true
					}
				`, testProject.PathWithNamespace),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckGitlabProjectLevelMRApprovalsExists("gitlab_project_level_mr_approvals.foo", &projectApprovals),
					testAccCheckGitlabProjectLevelMRApprovalsAttributes(&projectApprovals, &testAccGitlabProjectLevelMRApprovalsExpectedAttributes{
						resetApprovalsOnPush:                      false,
						disableOverridingApproversPerMergeRequest: true,
						mergeRequestsAuthorApproval:               true,
						mergeRequestsDisableCommittersApproval:    true,
						requirePasswordToApprove:                  true,
						selectiveCodeOwnerRemovals:                gitlab.Bool(true),
					}),
				),
			},
			{ //invert the settings, disabling selective code owner removal and setting reset approvals
				Config: fmt.Sprintf(`
					resource "gitlab_project_level_mr_approvals" "foo" {
						project                                        = "%s"
						reset_approvals_on_push                        = true
						disable_overriding_approvers_per_merge_request = true
						merge_requests_author_approval                 = true
						merge_requests_disable_committers_approval     = true
						require_password_to_approve                    = true
						selective_code_owner_removals                  = false
					}
				`, testProject.PathWithNamespace),
				Check: resource.ComposeTestCheckFunc(
					testAccCheckGitlabProjectLevelMRApprovalsExists("gitlab_project_level_mr_approvals.foo", &projectApprovals),
					testAccCheckGitlabProjectLevelMRApprovalsAttributes(&projectApprovals, &testAccGitlabProjectLevelMRApprovalsExpectedAttributes{
						resetApprovalsOnPush:                      true,
						disableOverridingApproversPerMergeRequest: true,
						mergeRequestsAuthorApproval:               true,
						mergeRequestsDisableCommittersApproval:    true,
						requirePasswordToApprove:                  true,
						selectiveCodeOwnerRemovals:                gitlab.Bool(false),
					}),
				),
			},
		},
	})
}

// pointer where the argument is optional, so it doesn't need to be provided and validated in every test.
type testAccGitlabProjectLevelMRApprovalsExpectedAttributes struct {
	resetApprovalsOnPush                      bool
	disableOverridingApproversPerMergeRequest bool
	mergeRequestsAuthorApproval               bool
	mergeRequestsDisableCommittersApproval    bool
	requirePasswordToApprove                  bool
	selectiveCodeOwnerRemovals                *bool
}

func testAccCheckGitlabProjectLevelMRApprovalsAttributes(projectApprovals *gitlab.ProjectApprovals, want *testAccGitlabProjectLevelMRApprovalsExpectedAttributes) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		if projectApprovals.ResetApprovalsOnPush != want.resetApprovalsOnPush {
			return fmt.Errorf("got reset_approvals_on_push %t; want %t", projectApprovals.ResetApprovalsOnPush, want.resetApprovalsOnPush)
		}
		if projectApprovals.DisableOverridingApproversPerMergeRequest != want.disableOverridingApproversPerMergeRequest {
			return fmt.Errorf("got disable_overriding_approvers_per_merge_request %t; want %t", projectApprovals.DisableOverridingApproversPerMergeRequest, want.disableOverridingApproversPerMergeRequest)
		}
		if projectApprovals.MergeRequestsAuthorApproval != want.mergeRequestsAuthorApproval {
			return fmt.Errorf("got merge_requests_author_approval %t; want %t", projectApprovals.MergeRequestsAuthorApproval, want.mergeRequestsAuthorApproval)
		}
		if projectApprovals.MergeRequestsDisableCommittersApproval != want.mergeRequestsDisableCommittersApproval {
			return fmt.Errorf("got merge_requests_disable_committers_approval %t; want %t", projectApprovals.MergeRequestsDisableCommittersApproval, want.mergeRequestsDisableCommittersApproval)
		}
		if projectApprovals.RequirePasswordToApprove != want.requirePasswordToApprove {
			return fmt.Errorf("got require_password_to_approve %t; want %t", projectApprovals.RequirePasswordToApprove, want.requirePasswordToApprove)
		}
		if want.selectiveCodeOwnerRemovals != nil && projectApprovals.SelectiveCodeOwnerRemovals != *want.selectiveCodeOwnerRemovals {
			return fmt.Errorf("got selective_code_owner_removals %t; want %t", projectApprovals.SelectiveCodeOwnerRemovals, *want.selectiveCodeOwnerRemovals)
		}
		return nil
	}
}

func testAccCheckGitlabProjectLevelMRApprovalsDestroy(s *terraform.State) error {
	for _, rs := range s.RootModule().Resources {
		if rs.Type != "gitlab_project" {
			continue
		}

		gotRepo, resp, err := testutil.TestGitlabClient.Projects.GetProject(rs.Primary.ID, nil)
		if err == nil {
			if gotRepo != nil && fmt.Sprintf("%d", gotRepo.ID) == rs.Primary.ID {
				if gotRepo.MarkedForDeletionAt == nil {
					return fmt.Errorf("Repository still exists")
				}
			}
		}
		if resp != nil && resp.StatusCode != 404 {
			return err
		}
		return nil
	}
	return nil
}

func testAccCheckGitlabProjectLevelMRApprovalsExists(n string, projectApprovals *gitlab.ProjectApprovals) resource.TestCheckFunc {
	return func(s *terraform.State) error {
		rs, ok := s.RootModule().Resources[n]
		if !ok {
			return fmt.Errorf("Not Found: %s", n)
		}

		projectId := rs.Primary.ID
		if projectId == "" {
			return fmt.Errorf("No project ID is set")
		}

		gotApprovalConfig, _, err := testutil.TestGitlabClient.Projects.GetApprovalConfiguration(projectId)
		if err != nil {
			return err
		}

		*projectApprovals = *gotApprovalConfig
		return nil
	}
}
